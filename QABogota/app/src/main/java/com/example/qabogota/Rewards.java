package com.example.qabogota;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Rewards extends AppCompatActivity {


    private TextView error;
    private TextView points;
    private String intentName;
    private String intentMail;
    private String intentPoints;
    private String intentId;
    private String intentProfile;
    private static String emergencyContact;
    private FirebaseFirestore mFirestore;
    private int totalPoints;

    private Rewards a = this;
    private LinearLayout linearlayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewards);
        Intent intent = getIntent();
        mFirestore=FirebaseFirestore.getInstance();
        intentName = intent.getStringExtra("name");
        intentMail = intent.getStringExtra("mail");
        intentPoints = intent.getStringExtra("points");
        intentId = intent.getStringExtra("id");
        emergencyContact = intent.getStringExtra("emergency");
        intentProfile = intent.getStringExtra("profile");
        points = findViewById(R.id.rPoints);
        points.setText(intentPoints);
        error = findViewById(R.id.errorRewards);
        error.setVisibility(View.INVISIBLE);
        linearlayout=findViewById(R.id.linearlayout);
        ConnectivityManager cm =
                (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected) {
            mFirestore = FirebaseFirestore.getInstance();
            CollectionReference citiesRef = mFirestore.collection("Rewards");

            citiesRef.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful() && !task.getResult().isEmpty()) {
                    int i = 0;
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String name = (String) document.getData().get("name");
                            final long points2 = (long) document.getData().get("points");
                            CardView card= new CardView(new ContextThemeWrapper(a, R.style.Card),null,0);
                            TextView t= new TextView(a);
                            Button button = new Button(new ContextThemeWrapper(a, R.style.Button),null,0);
                            button.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ConnectivityManager cm =
                                            (ConnectivityManager)a.getSystemService(Context.CONNECTIVITY_SERVICE);

                                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                                    final boolean isConnected = activeNetwork != null &&
                                            activeNetwork.isConnectedOrConnecting();
                                    if(isConnected){
                                        totalPoints=Integer.parseInt(intentPoints);
                                        if(totalPoints<points2) {
                                            AlertDialog.Builder builder1 = new AlertDialog.Builder(a);
                                            builder1.setMessage("You don't have enough points");
                                            builder1.setCancelable(true);

                                            builder1.setPositiveButton(
                                                    "I understand",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.cancel();
                                                        }
                                                    });

                                            AlertDialog alert11 = builder1.create();
                                            alert11.show();
                                        }
                                        else{
                                            AlertDialog.Builder builder1 = new AlertDialog.Builder(a);
                                            builder1.setMessage("Are you sure you want to use your points for this reward?");
                                            builder1.setCancelable(true);

                                            builder1.setPositiveButton(
                                                    "Yes",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            totalPoints-=points2;
                                                            Map<String, Object> userMap = new HashMap<String, Object>();
                                                            userMap.put("points",totalPoints);
                                                            mFirestore.collection("Users")
                                                                    .document(intentId)
                                                                    .update(userMap);
                                                            updatePoints(totalPoints+"");
                                                            intentPoints=totalPoints+"";
                                                            Intent qr = new Intent(a, qr.class);
                                                            startActivity(qr);
                                                        }});

                                            builder1.setNegativeButton(
                                                    "No",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                            dialog.cancel();
                                                        }
                                                    });

                                            AlertDialog alert11 = builder1.create();
                                            alert11.show();
                                        }
                                    }
                                    else
                                    {
                                        error.setText("Internet Connection needed");
                                        error.setVisibility(View.VISIBLE);
                                    }
                                }
                            });
                            t.setText(name);
                            t.setTextAppearance(a,R.style.Card_title);
                            TextView t2= new TextView(a);
                            t2.setText(points2 +"");
                            t2.setTextAppearance(a,R.style.Card_body);
                            LinearLayout cardlayout= new LinearLayout(a);
                            cardlayout.setOrientation(LinearLayout.VERTICAL);
                            button.setText("Claim");
                            cardlayout.addView(t);
                            cardlayout.addView(t2);
                            cardlayout.addView(button);
                            card.addView(cardlayout);
                            linearlayout.addView(card);
                        }
                    }
                }
            });
        }

    }

    public void updatePoints(String points2){
        points.setText(points2);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        ConnectivityManager cm =
                (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        final boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected){
            mFirestore=FirebaseFirestore.getInstance();
            DocumentReference citiesRef = mFirestore.collection("Users").document(intentId);
            citiesRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            long pointst=(long)document.getData().get("points");
                            points.setText(pointst+"");
                            intentPoints=pointst+"";
                        }
                    }
                }
            });

        }
    }


}

