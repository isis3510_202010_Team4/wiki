package com.example.qabogota;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class SQLiteData extends AppCompatActivity {

    private ListView listview;

    private ArrayList<String> list;

    private String repo="Report \n";

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    /**
     * Checks if the app has permission to write to device storage
     *
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                1);
    }

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_data);

        update();
    }
    @Override
    public void onResume(){
        super.onResume();
    }


    public void update()
    {
        list = new ArrayList<String>();
        AdminSQLiteOpenHelper admin =new AdminSQLiteOpenHelper(this, "data4", null, 1);
        SQLiteDatabase database = admin.getWritableDatabase();
        Cursor cursor = database.rawQuery("select * from contamination_data4",null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String name = "Air quality: "+cursor.getString(1)  + " UV radiation: " +cursor.getString(2) + " Latitude: " + cursor.getString(3) + " Longitude: " + cursor.getString(4) + " Timestamp: " + cursor.getString(5);
                repo+=cursor.getString(1) +";" +cursor.getString(2) +";" + cursor.getString(3) + ";" +cursor.getString(4)+ ";" +cursor.getString(5) + "\n";
                list.add(name);
                cursor.moveToNext();
            }
        }
        database.close();
        listview =  findViewById(R.id.lv);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
        listview.setAdapter(adapter);
    }
    public void deleteRows(View view)
    {
        AdminSQLiteOpenHelper admin =new AdminSQLiteOpenHelper(this, "data4", null, 1);
        SQLiteDatabase database = admin.getWritableDatabase();
        database.rawQuery("delete from contamination_data4",null);
        database.close();
        update();
    }

    public void report(View view){
        verifyStoragePermissions(this);
        generateNoteOnSD(this,"report.txt",repo);
    }

    private void generateNoteOnSD(Context context, String sFileName, String sBody) {
        try {
            File gpxfile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), sFileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
            Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
