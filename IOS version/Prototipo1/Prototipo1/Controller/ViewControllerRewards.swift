//
//  ViewControllerRewards.swift
//  Prototipo1
//
//  Created by David Patiño on 26/04/20.
//  Copyright © 2020 DAVID PATIÐO MONTOYA. All rights reserved.
//

import UIKit

class ViewControllerRewards: UIViewController {
    
    var points:String? = ""
    let reachability = try! Reachability()
    
    @IBOutlet weak var TableViewRewards: UITableView!
    
    var rewards = [["name":"reward 1","description":"Gasoline discount","cost": 1200],["name":"reward 2","description":"Technical discount","Cost": 3000],["name":"reward 3","description":"Free subscription","cost": 5000]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(points)
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buttonPressed (_ sender: UIButton ){
        print(reachability.connection)
        switch reachability.connection {
        case .wifi, .cellular:
            var cost = 0
            var finalPoints = 0
            if (sender.tag == 0){
                cost=1200
            }
            else if (sender.tag == 1){
                cost=3000
            }
            else{
                cost = 5000
            }
            let points2:String = points!
            finalPoints = Int(points2)!  - cost
            print(finalPoints)
            
            if (finalPoints>=0){
                let alert = UIAlertController(title: "Rewards", message: "Congratulations! Enjoy your reward", preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
            else{
                let alert = UIAlertController(title: "Rewards", message: "Unfortunally your points are not enought for the price. Keep driving to earn it!", preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
            
        case .unavailable:
            print("Network not reachable")
            let alert = UIAlertController(title: "Service Unavilable!", message: "Unfortunally you dont have internet connection, try again later!", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        case .none:
            print("Error")
        }
        
        
    }
    
    func loadTable(){
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
