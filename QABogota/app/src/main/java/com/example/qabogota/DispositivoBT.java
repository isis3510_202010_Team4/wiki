package com.example.qabogota;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Set;

public class DispositivoBT extends AppCompatActivity {

    private static final String TAG = "DispositivoBT";
    ListView list;
    public static String EXTRA_DEVICE_ADDRESS="device_address";
    private BluetoothAdapter mBtAdapter;
    private ArrayAdapter<String> mPairedDevicesArrayAdapter;
    private String intentName;
    private String intentMail;
    private String intentPoints;
    private String intentId;
    private static String emergencyContact;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dispositivo_b_t);
        Intent intent = getIntent();
        intentName = intent.getStringExtra("name");
        intentMail = intent.getStringExtra("mail");
        intentPoints = intent.getStringExtra("points");
        intentId = intent.getStringExtra("id");
        emergencyContact = intent.getStringExtra("emergency");
    }

    @Override
    public void onResume(){
        super.onResume();

        mPairedDevicesArrayAdapter = new ArrayAdapter<String>(this,R.layout.nombre_dispostivios);
        list = (ListView)  findViewById(R.id.idList);
        list.setAdapter(mPairedDevicesArrayAdapter);
        list.setOnItemClickListener(mDeviceClickListener);
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();
        if (pairedDevices.size()>0)
        {
            for(BluetoothDevice device: pairedDevices)
            {
                mPairedDevicesArrayAdapter.add(device.getName() + "\n" + device.getAddress());
            }
        }
    }
    private AdapterView.OnItemClickListener mDeviceClickListener = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView av, View v, int arg2, long arg3) {
            String info = ((TextView) v).getText().toString();
            String address = info.substring(info.length() - 17);

            Intent i = new Intent(DispositivoBT.this, Profile.class);
            i.putExtra(EXTRA_DEVICE_ADDRESS, address);
            i.putExtra("name",intentName);
            i.putExtra("mail",intentMail);
            i.putExtra("points",intentPoints);
            i.putExtra("emergency",emergencyContact);
            i.putExtra("id",intentId);
            startActivity(i);
        }
    };

}
