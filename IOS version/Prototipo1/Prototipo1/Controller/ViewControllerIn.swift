//
//  ViewControllerIn.swift
//  Prototipo1
//
//  Created by David Patiño on 26/04/20.
//  Copyright © 2020 DAVID PATIÐO MONTOYA. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation
import MessageUI
import GooglePlaces
import GoogleMaps


class ViewControllerIn: UIViewController{
    

    @IBOutlet weak var MapView: GMSMapView!
    @IBOutlet weak var NameLable: UILabel!
    @IBOutlet weak var UserPoints: UITextField!
    var numberTelephone = ""
    let db = Firestore.firestore()
    let reachability = try! Reachability()
    let locationManager = CLLocationManager()
    var lat = 0.0
    var lon = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        navigationItem.hidesBackButton = true
        locationManager.requestWhenInUseAuthorization()
        // Do any additional setup after loading the view.
        
        //maps
        MapView.settings.compassButton = true
        MapView.settings.scrollGestures = true
        MapView.settings.allowScrollGesturesDuringRotateOrZoom = true
        MapView.settings.zoomGestures = true
        MapView.isMyLocationEnabled = true
        MapView.settings.myLocationButton = true
        
        
        //user 
        
        db.collection(K.db.users).whereField("mail", isEqualTo: Auth.auth().currentUser?.email)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    
                    print("Error getting documents: \(err)")
                } else {
                    let document = querySnapshot!.documents[0]
                    
                    //self.UserName.text = document["name"] as? String
                    //self.UserEmail.text = document["mail"] as? String
                    self.NameLable.text = document["name"] as? String
                    self.UserPoints.text = String(document["points"] as? Int ?? 0)
                    self.numberTelephone = String(document["ContactPhone"] as? Int ?? 123)
                    
                    
                    
                }
                Timer.scheduledTimer(withTimeInterval: 3, repeats: true) { (timer) in
                    self.locationManager.requestLocation()
                }
        }
        // points get
        db.collection(K.db.points)
            .getDocuments() { (querySnapshot, err) in
                if let err = err {
                    
                    print("Error getting documents: \(err)")
                } else {
                    let documents = querySnapshot!.documents
                    print("Se cogieron los documentos")
                    for point in documents{
                        
                        let lat = point["lat"] as? Double ?? 0
                        let lon = point["lon"] as? Double ?? 0
                        let val = point["val"] as? String ?? "Error"
                        print(String(lat)+String(lon)+val)
                        let circleCenter = CLLocationCoordinate2D(latitude: lat, longitude: lon)
                        let circ = GMSCircle(position: circleCenter, radius: 1000)
                        switch val {
                        case "h":
                            circ.fillColor = UIColor(red: 0.9176, green: 0.3294, blue: 0.3294, alpha: 0.3)
              
                        case "m":
                            circ.fillColor = UIColor(red: 0.9176, green: 0.6627, blue: 0.3294, alpha: 0.2)
                        case "l":
                            circ.fillColor = UIColor(red: 0.6235, green: 0.9176, blue: 0.3294, alpha: 0.2)
            
                        default:
                            print("Error getting documents:")
                        }
                        circ.strokeWidth = 0
                        
                        circ.map = self.MapView
                        
                    }
                    
                    
                }
                
        }
        
    }
    
    @IBAction func sendPanic(_ sender: UIButton) {
        print("Se envia mensaje")
        displayMessageInterface()
    }
    
    func displayMessageInterface() {
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.recipients = [numberTelephone]
        composeVC.body = "Emergency!"
        
        // Present the view controller modally.
        if MFMessageComposeViewController.canSendText() {
            print("se envio")
            self.present(composeVC, animated: true, completion: nil)
        } else {
            print("Can't send messages.")
        }
    }
    
    func recordData(){
        switch reachability.connection {
        case .wifi, .cellular:
            print("send via wifi")
            
            db.collection(K.db.data).addDocument(data: [K.dbFields.dataAq: Int.random(in: 0 ..< 10), K.dbFields.dataUv: Int.random(in: 0 ..< 10), K.dbFields.dataTime: Date().timeIntervalSince1970, K.dbFields.dataLatitude: self.lat, K.dbFields.dataLongitude: self.lon]) { (error) in
                if let e = error {

                    print(e.localizedDescription)
                }

            }
            
            print("Se registro")
    
        case .unavailable:
            let lat = self.lon
            let long = self.lat
            let UV = Int.random(in: 0 ..< 10)
            let co2 = Int.random(in: 0 ..< 10)
            print(lat)
            print(long)
            print(UV)
            print(co2)
            let observacion =  Observacion(context: AppDelegate.context)
            observacion.co2 = Float(co2)
            observacion.uv = Float (UV)
            observacion.lat = Float (lat)
            observacion.long = Float (long)
            
            AppDelegate.saveContext()
            
        case .none:
            print("Error")
        }
        
        
        
    }
    
    @IBAction func logOut(_ sender: Any) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            navigationController?.popToRootViewController(animated: true)
        } catch let e as NSError {
            print (e.localizedDescription)
            
        }
    }
    
    @IBAction func RewardsButton(_ sender: UIButton) {
        self.performSegue(withIdentifier: K.segues.inToRewards, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is ViewControllerRewards {
            let vc = segue.destination as? ViewControllerRewards
            vc?.points = self.UserPoints.text
            
            
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
//MARK: CLLocationManagerDelegate
extension ViewControllerIn: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            self.lat=location.coordinate.latitude
            self.lon=location.coordinate.longitude
            print("estoy aca coordenadas lat:"+String(self.lat)+" lon: "+String(self.lon))
            //recordData()
        }
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
}

//MARK:
extension ViewControllerIn: MFMessageComposeViewControllerDelegate{
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        
    }
    

}
