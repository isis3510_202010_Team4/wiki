package com.example.qabogota;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;


public class MainActivity extends AppCompatActivity {


    private TextView error;
    private boolean isConnected;
    private FirebaseFirestore mFirestore;
    private EditText email;
    private EditText pass;
    private MainActivity a = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        email = findViewById(R.id.editTextTextEmailAddress);
        pass = findViewById(R.id.editTextTextPassword);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.SEND_SMS},
                    1);
        }

        error = findViewById(R.id.errorMain);
        error.setVisibility(View.INVISIBLE);

    }

    public void logIn(View view)
    {
        String mail = email.getText().toString();
        String pass2 = pass.getText().toString();
        ConnectivityManager cm =
                (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected){
            mFirestore=FirebaseFirestore.getInstance();
            CollectionReference citiesRef = mFirestore.collection("Users");

            citiesRef.whereEqualTo("mail",mail).whereEqualTo("pass",pass2).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful() && !task.getResult().isEmpty()) {
                        String id= "";
                        String name= "";
                        String mail = "";
                        long points = 0;
                        long emergency= 0;
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            name = (String)document.getData().get("name");
                            mail = (String)document.getData().get("mail");
                            points = (long)document.getData().get("points");
                            emergency = (long)document.getData().get("number");
                            id = document.getId();
                        }
                        Intent profileNBT = new Intent(a, ProfileNBT.class);
                        profileNBT.putExtra("name",name);
                        profileNBT.putExtra("mail",mail);
                        profileNBT.putExtra("points",points+"");
                        profileNBT.putExtra("emergency",emergency+"");
                        profileNBT.putExtra("id",id);

                        startActivity(profileNBT);
                    } else {
                        error.setVisibility(View.VISIBLE);
                        error.setText("Mail or password are incorrect");
                    }
                }
            });


        }
        else{
            error.setVisibility(View.VISIBLE);
            error.setText("Internet Connection needed");
        }
    }
    public void register(View view)
    {
        ConnectivityManager cm =
                (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected){
            Intent reg = new Intent(this, Register.class);
            startActivity(reg);
        }
        else{
            error.setVisibility(View.VISIBLE);
            error.setText("Internet Connection needed");
        }
    }
}
