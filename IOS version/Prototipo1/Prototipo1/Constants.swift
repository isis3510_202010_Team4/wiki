//
//  Constants.swift
//  Prototipo1
//
//  Created by David Patiño on 26/04/20.
//  Copyright © 2020 DAVID PATIÐO MONTOYA. All rights reserved.
//

import Foundation

struct K {
    struct segues {
        static let loginToIn = "loginToIn"
        static let registerToIn = "registerToIn"
        static let inToData = "inToData"
        static let inToWarning = "inToWarning"
        static let inToRewards = "inToRewards"
    }
    struct db {
        static let users = "Users"
        static let data = "DataiOS"
        static let points = "PointsiOS"
        
    }
    struct dbFields {
        static let usersMail = "mail"
        static let usersPhone = "name"
        static let usersName = "number"
        static let usersPass = "pass"
        static let usersPoints = "points"
        static let usersContactName = "ContactName"
        static let usersContactPhone = "ContactPhone"
        static let dataAq = "aq"
        static let dataLatitude = "latitude"
        static let dataLongitude = "longitude"
        static let dataTime = "timestamp"
        static let dataUv = "uv"
        
    }
}
