package com.example.qabogota;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class Register extends AppCompatActivity {

    private TextView error;
    private EditText name;
    private EditText email;
    private EditText pass;
    private EditText passC;
    private EditText numberT;
    private FirebaseFirestore mFirestore;
    private boolean isConnected;
    private Register a = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        error = findViewById(R.id.errorRegistro);
        error.setVisibility(View.INVISIBLE);
        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        pass = findViewById(R.id.password);
        passC = findViewById(R.id.passwordC);
        numberT= findViewById(R.id.editTextPhone);

    }

    public void cancel(View view)
    {
        Intent main = new Intent(this, MainActivity.class);
        startActivity(main);
    }

    public void submit(View view)
    {
        final String n = name.getText().toString();
        final String e = email.getText().toString();
        final String p = pass.getText().toString();
        String pC = passC.getText().toString();
        String phone=numberT.getText().toString();
        if(n.equals("")) {
            error.setText("Name's empty");
            error.setVisibility(View.VISIBLE);
        }
        else if(e.equals("")) {
            error.setText("Email's empty");
            error.setVisibility(View.VISIBLE);
        }
        else if(p.equals("")) {
            error.setText("Password's empty");
            error.setVisibility(View.VISIBLE);
        }
        else if(pC.equals("")) {
            error.setText("Confirmation's empty");
            error.setVisibility(View.VISIBLE);
        }
        else if(!pC.equals(p))
        {
            error.setText("Passwords don't match");
            error.setVisibility(View.VISIBLE);
        }
        else if(phone.equals("")) {
            error.setText("Emergency number is empty");
            error.setVisibility(View.VISIBLE);
        }
        else
        {
                final long numberint= Long.parseLong(phone);
                ConnectivityManager cm =
                        (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();
                if(isConnected){
                    mFirestore=FirebaseFirestore.getInstance();
                    CollectionReference citiesRef = mFirestore.collection("Users");
                    citiesRef.whereEqualTo("mail",e).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful() && task.getResult().isEmpty()) {
                                User u = new User(n,e,p,numberint,0);
                                error.setVisibility(View.INVISIBLE);
                                CollectionReference dbData = mFirestore.collection("Users");
                                dbData.add(u);
                                name.setText("");
                                email.setText("");
                                pass.setText("");
                                passC.setText("");
                                Intent main = new Intent(a, MainActivity.class);
                                startActivity(main);
                            } else {
                                error.setVisibility(View.VISIBLE);
                                error.setText("There is already an user with that mail");
                            }
                        }
                    });

                }
                else{
                    error.setText("Internet Connection needed");
                    error.setVisibility(View.VISIBLE);
                }
        }
    }
}
