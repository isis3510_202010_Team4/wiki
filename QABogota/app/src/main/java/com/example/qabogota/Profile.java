package com.example.qabogota;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.icu.util.Output;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class Profile extends AppCompatActivity {

    private FusedLocationProviderClient fusedLocationClient;
    private static double latitude;
    private static double longitude;
    private TextView yp;
    private TextView aq;
    private TextView uv;
    private TextView error;
    private int clickCount;
    Handler bluetoothIn;
    final int handlerState = 0;
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private StringBuilder DataStringIN = new StringBuilder();
    private ConnectedThread MyConnectionBT;
    private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static String address = null;
    private String intentName;
    private String intentMail;
    private String intentPoints;
    private String intentId;
    private TextView name;
    private TextView mail;
    private TextView points;
    private static String emergencyContact;
    private FirebaseFirestore mFirestore;
    private int count=0;
    private int totalPoints;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Intent intent = getIntent();
        name = findViewById(R.id.nName);
        intentName = intent.getStringExtra("name");
        name.setText(intentName);
        mail = findViewById(R.id.nMail);
        intentMail = intent.getStringExtra("mail");
        mail.setText(intentMail);
        points = findViewById(R.id.nPoints);
        intentPoints = intent.getStringExtra("points");
        totalPoints=Integer.parseInt(intentPoints);
        points.setText(intentPoints);
        intentId = intent.getStringExtra("id");
        emergencyContact = intent.getStringExtra("emergency");
        count ++;
        yp = findViewById(R.id.yourPoints);
        aq = findViewById(R.id.aqLevel);
        uv = findViewById(R.id.uvLevel);
        mFirestore=FirebaseFirestore.getInstance();
        clickCount=0;
        error = findViewById(R.id.errorProfile);
        error.setVisibility(View.INVISIBLE);
        ConnectivityManager cm =
                (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        final boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        bluetoothIn = new Handler() {
            public void handleMessage(android.os.Message msg) {
                if (msg.what == handlerState) {
                    String readMessage = (String) msg.obj;
                    DataStringIN.append(readMessage);

                    int endOfLineIndex = DataStringIN.indexOf("#");

                    if (endOfLineIndex > 0) {
                        String dataInPrint = DataStringIN.substring(0, endOfLineIndex);
                        String[] aux = dataInPrint.split(",");
                        aq.setText(aux[0]);
                        uv.setText(aux[1]);
                        gps();
                        double aqdoub = Double.parseDouble(aux[0]);
                        double uvdoub = Double.parseDouble(aux[1]);
                        Register(aqdoub,uvdoub,latitude,longitude);
                        if(aqdoub>70)
                        {
                            alert("AQ");
                        }
                        else if(uvdoub>6)
                        {
                            alert("UV");
                        }
                        if(isConnected){
                            addData();
                        }
                        DataStringIN.delete(0, DataStringIN.length());
                    }
                }
                Map<String, Object> userMap = new HashMap<String, Object>();
                userMap.put("points",totalPoints);
                mFirestore.collection("Users")
                        .document(intentId)
                        .update(userMap);
                points.setText(totalPoints+"");
                intentPoints=totalPoints+"";
            }
        };
        btAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    private void addData(){
        AdminSQLiteOpenHelper admin =new AdminSQLiteOpenHelper(this, "data4", null, 1);
        SQLiteDatabase database = admin.getWritableDatabase();
        Cursor cursor = database.rawQuery("select * from contamination_data4",null);
        CollectionReference dbData = mFirestore.collection("Data");
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                try {
                    totalPoints+=1;
                    Date date1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(cursor.getString(5));
                    Data data = new Data(Double.parseDouble(cursor.getString(1)),Double.parseDouble(cursor.getString(2)),Double.parseDouble(cursor.getString(3)),Double.parseDouble(cursor.getString(4)),date1);
                    dbData.add(data);
                    cursor.moveToNext();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        database.execSQL("delete from contamination_data4");
        database.close();
        intentPoints = totalPoints + "";
    }

    private void Register(double aq, double uv, double latitude, double longitude){
        AdminSQLiteOpenHelper admin = new AdminSQLiteOpenHelper(this, "data4",null,1);
        SQLiteDatabase database = admin.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("AQ",aq);
        cv.put("UV",uv);
        cv.put("latitude",latitude);
        cv.put("longitude",longitude);
        database.insert("contamination_data4",null, cv);
        database.close();
    }

    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        LocationRequest loc = new LocationRequest();
        loc.setInterval(20000);
        LocationCallback lob = new LocationCallback();
        fusedLocationClient.requestLocationUpdates(loc,lob,null);
    }

    @SuppressLint("MissingPermission")
    private void stopLocationUpdates() {
        LocationRequest loc = new LocationRequest();
        loc.setInterval(20000);
        LocationCallback lob = new LocationCallback();
        fusedLocationClient.removeLocationUpdates(lob);
    }


    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException
    {
        return device.createRfcommSocketToServiceRecord(BTMODULEUUID);
    }

    @Override
    public void onResume()
    {
        startLocationUpdates();
        super.onResume();
        Intent intent = getIntent();
        address = intent.getStringExtra(DispositivoBT.EXTRA_DEVICE_ADDRESS);
        BluetoothDevice device = btAdapter.getRemoteDevice(address);

        try
        {
            btSocket = createBluetoothSocket(device);
        } catch (IOException e) {
            Toast.makeText(getBaseContext(), "La creacción del Socket fallo", Toast.LENGTH_LONG).show();
        }
        try
        {
            btSocket.connect();
        } catch (IOException e) {
            try {
                btSocket.close();
            } catch (IOException e2) {}
        }
        MyConnectionBT = new ConnectedThread(btSocket);
        MyConnectionBT.start();
        ConnectivityManager cm =
                (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        final boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected){
            mFirestore=FirebaseFirestore.getInstance();
            DocumentReference citiesRef = mFirestore.collection("Users").document(intentId);
            citiesRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            String namet=(String)document.getData().get("name");
                            String mailt=(String)document.getData().get("mail");
                            long pointst=(long)document.getData().get("points");
                            name.setText(namet);
                            mail.setText(mailt);
                            points.setText(pointst+"");
                        } else {

                        }
                    } else {

                    }
                }
            });

        }
    }

    private class ConnectedThread extends Thread
    {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;

        public ConnectedThread(BluetoothSocket socket)
        {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            try
            {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (IOException e) { }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run()
        {
            byte[] buffer = new byte[256];
            int bytes;

            while (true) {
                try {
                    bytes = mmInStream.read(buffer);
                    String readMessage = new String(buffer, 0, bytes);
                    bluetoothIn.obtainMessage(handlerState, bytes, -1, readMessage).sendToTarget();
                } catch (IOException e) {
                    break;
                }
            }
        }
    }

    @Override
    public void onPause() {

        super.onPause();
    }
    public void panic(View v) {
        clickCount++;
        if(clickCount==2)
        {
            gps();
            sendMessage();
            clickCount=0;
        }
    }

    public void sendMessage(){
        SmsManager smsManager = SmsManager.getDefault();
        String message = "I'm in danger! \n Latitude: " + latitude + "\n Longitude: " + longitude;
        smsManager.sendTextMessage(emergencyContact, null, message, null, null);
    }

    public void alert(String reason)
    {
        Intent warning = new Intent(this, Warning.class);
        warning.putExtra("reason",reason);
        startActivity(warning);
    }

    public void logOut(View view)
    {
        Intent logOut = new Intent(this, MainActivity.class);
        startActivity(logOut);
    }

    public void viewData(View view)
    {
        Intent viewdata = new Intent(this, SQLiteData.class);
        startActivity(viewdata);
    }

    public void rewards(View view)
    {
        Intent rw = new Intent(this, Rewards.class);
        rw.putExtra("name",intentName);
        rw.putExtra("mail",intentMail);
        rw.putExtra("points",intentPoints);
        rw.putExtra("emergency",emergencyContact);
        rw.putExtra("id",intentId);
        rw.putExtra("profile","p");
        rw.putExtra("address", address);
        startActivity(rw);
    }

    @SuppressLint("MissingPermission")
    public void gps() {
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                });
    }

}
