//
//  ViewControllerRegister.swift
//  Prototipo1
//
//  Created by David Patiño on 26/04/20.
//  Copyright © 2020 DAVID PATIÐO MONTOYA. All rights reserved.
//

import UIKit
import Firebase

class ViewControllerRegister: UIViewController {

    @IBOutlet weak var NameFieldRegister: UITextField!
    @IBOutlet weak var EmailFieldRegister: UITextField!
    @IBOutlet weak var PhoneFieldRegister: UITextField!
    
    @IBOutlet weak var PasswrdFieldRegister: UITextField!
    
    @IBOutlet weak var ContactNameFieldRegister: UITextField!
    
    @IBOutlet weak var ContactNumberRegister: UITextField!
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func register(_ sender: UIButton) {
      
        if let email = EmailFieldRegister.text, !email.isEmpty, let password = PasswrdFieldRegister.text, !password.isEmpty, let name = NameFieldRegister.text, !name.isEmpty, let phone = PhoneFieldRegister.text, !phone.isEmpty, let contactName = ContactNameFieldRegister.text, !contactName.isEmpty , let contactPhone = ContactNumberRegister.text, !contactPhone.isEmpty {
            Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
              // ...
                if let e = error{
                    let alertController = UIAlertController(title: "The operation was not completed!", message: e.localizedDescription, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alertController.addAction(okAction)
                    self.present(alertController,animated: true, completion: nil )
                    
                    print(e.localizedDescription)
                }else{
                    if name.count>30{
                        let alertController = UIAlertController(title: "The operation was not completed!", message: "the name exceded the maximum characters!", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                        alertController.addAction(okAction)
                        self.present(alertController,animated: true, completion: nil )
                    }
                    else if contactName.count>30{
                            let alertController = UIAlertController(title: "The operation was not completed!", message: "the password exceded the maximum characters!", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                            alertController.addAction(okAction)
                            self.present(alertController,animated: true, completion: nil )
                    }
                    else if contactPhone.count>30{
                            let alertController = UIAlertController(title: "The operation was not completed!", message: "the password exceded the maximum characters!", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                            alertController.addAction(okAction)
                            self.present(alertController,animated: true, completion: nil )
                    }
                    else if password.count>30{
                        let alertController = UIAlertController(title: "The operation was not completed!", message: "the password exceded the maximum characters!", preferredStyle: .alert)
                        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                        alertController.addAction(okAction)
                        self.present(alertController,animated: true, completion: nil )
                    }else{
                    
                        self.db.collection(K.db.users).addDocument(data: [K.dbFields.usersMail: String(email), K.dbFields.usersPass: String(password), K.dbFields.usersName: String(name), K.dbFields.usersPhone: Int(phone) ?? 0, K.dbFields.usersPoints: 0, K.dbFields.usersContactName: String(contactName), K.dbFields.usersContactPhone: Int(contactPhone) ?? 0]) { (error) in
                            if let e = error {
                                let alertController = UIAlertController(title: "The operation was not completed!", message: e.localizedDescription, preferredStyle: .alert)
                                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                                alertController.addAction(okAction)
                                self.present(alertController,animated: true, completion: nil )
                                print(e.localizedDescription)
                            }
                            
                        }
                        print("Se registro")
                        self.performSegue(withIdentifier: K.segues.registerToIn, sender: self)
                    }
                }
            }
            
        }else{
            let alertController = UIAlertController(title: "The operation was not completed!", message: "Please fill all the fields", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController,animated: true, completion: nil )
        }
        
    }
    

}
