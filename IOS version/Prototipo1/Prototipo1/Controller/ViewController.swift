//
//  ViewController.swift
//  Prototipo1
//
//  Created by DAVID PATIÐO MONTOYA on 28/02/20.
//  Copyright © 2020 DAVID PATIÐO MONTOYA. All rights reserved.
//

import UIKit
import UserNotifications
import CoreBluetooth
import CoreData
import Firebase
import GoogleMaps
import GooglePlaces



class ViewController: UIViewController {
    
    @IBOutlet weak var EmailField: UITextField!
    @IBOutlet weak var PasswrdField: UITextField!
    
    let reachability = try! Reachability()
    
    
    var observations = [Observacion]()
    
    override func viewDidLoad() {
        //NotifyText.text = ""
        // Key google
        GMSServices.provideAPIKey("AIzaSyBkP54sO62zuqELOWeincYD9WXgzRdFhNU")
        GMSPlacesClient.provideAPIKey("AIzaSyBkP54sO62zuqELOWeincYD9WXgzRdFhNU")
        super.viewDidLoad()
        
        
        
    }
    
    @IBAction func singIn(_ sender: UIButton) {
        if let email = EmailField.text, let password = PasswrdField.text{
            Auth.auth().signIn(withEmail: email, password: password) { authResult, error in
                if let e = error {
                    let alertController = UIAlertController(title: "The operation was not completed!", message: e.localizedDescription, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                    alertController.addAction(okAction)
                    self.present(alertController,animated: true, completion: nil )
                    
                    print(e.localizedDescription)
                    
                }else{
                    print("Se inicio sesion")
                    self.performSegue(withIdentifier: K.segues.loginToIn, sender: self)
                    
                }
            }
        }
    }
    
    
//    @objc func reachabilityChanged(note: Notification) {
//
//      let reachability = note.object as! Reachability
//
//      switch reachability.connection {
//      case .wifi:
//        NotifyText.text="Reachable via WiFi"
//        NotifyText.backgroundColor=#colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
//      case .cellular:
//        NotifyText.text="Reachable via Cellular"
//        NotifyText.backgroundColor=#colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
//      case .unavailable:
//        NotifyText.text="Network not reachable"
//        NotifyText.backgroundColor=#colorLiteral(red: 0.1764705926, green: 0.01176470611, blue: 0.5607843399, alpha: 1)
//      case .none:
//        print("Error")
//        }
//    }
//
//    @IBAction func action(_ sender: Any) {
//        let content = UNMutableNotificationContent()
//        content.title = "Alerta enviada"
//        content.subtitle = "Alerta enviada"
//        content.body = "Alerta enviada"
//        content.badge = 1
//
//        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
//        let request = UNNotificationRequest(identifier: "timerDone", content: content, trigger: trigger)
//
//        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
//
//    }
//
//    @IBAction func guardar(_ sender: Any){
//
//        switch reachability.connection {
//        case .wifi:
//            print("send via wifi")
//            sendData()
//        case .cellular:
//            print("send via cellular")
//            sendData()
//        case .unavailable:
//            print("save in local storage")
//          let lat = 4.606
//          let long = -74.071
//          let UV = Int.random(in: 0 ..< 10)
//          let co2 = Int.random(in: 0 ..< 10)
//          print(lat)
//          print(long)
//          print(UV)
//          print(co2)
//          let observacion =  Observacion(context: AppDelegate.context)
//          observacion.co2 = Float(co2)
//          observacion.uv = Float (UV)
//          observacion.lat = Float (lat)
//          observacion.long = Float (long)
//
//          AppDelegate.saveContext()
//          self.observations.append(observacion)
//          self.tableView.reloadData()
//        case .none:
//          print("Error")
//          }
//    }
//
//    func sendData(){
//        let lat = 4.606
//        let long = -74.071
//        let UV = Int.random(in: 0 ..< 10)
//        let co2 = Int.random(in: 0 ..< 10)
//        print(lat)
//        print(long)
//        print(UV)
//        print(co2)
//        let observacion =  Observacion(context: AppDelegate.context)
//        observacion.co2 = Float(co2)
//        observacion.uv = Float (UV)
//        observacion.lat = Float (lat)
//        observacion.long = Float (long)
//
//        AppDelegate.saveContext()
//        self.observations.append(observacion)
//        self.tableView.reloadData()
//
//    }
//
//
//    func  createAlert (title: String, message: String){
//
//
//    }
//
//
//}
//
//extension ViewController: UITableViewDataSource{
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return observations.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
//        cell.textLabel?.text = String("Lat ")+String(observations[indexPath.row].lat)
//                                  + String(" Long: ")+String(observations[indexPath.row].long)
//        cell.detailTextLabel?.text=String("UV: ")+String (observations[indexPath.row].uv) + String(" CO2: ")+String(observations[indexPath.row].co2)
//
//        return cell
//    }
//
//
}
