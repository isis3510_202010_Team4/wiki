package com.example.qabogota;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class ProfileNBT extends AppCompatActivity {

    private FusedLocationProviderClient fusedLocationClient;
    private static double latitude;
    private static double longitude;
    private TextView error;
    private int clickCount;
    private FirebaseFirestore mFirestore;
    private String intentName;
    private String intentMail;
    private String intentPoints;
    private String intentId;
    private static String emergencyContact;
    private TextView name;
    private TextView mail;
    private TextView points;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_n_b_t);
        mFirestore=FirebaseFirestore.getInstance();
        clickCount=0;
        error = findViewById(R.id.errorProfile);
        error.setVisibility(View.INVISIBLE);
        ConnectivityManager cm =
                (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        final boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        Intent intent = getIntent();
        name = findViewById(R.id.nName);
        intentName = intent.getStringExtra("name");
        name.setText(intentName);
        mail = findViewById(R.id.nMail);
        intentMail = intent.getStringExtra("mail");
        mail.setText(intentMail);
        points = findViewById(R.id.nPoints);
        intentPoints = intent.getStringExtra("points");
        points.setText(intentPoints);
        intentId = intent.getStringExtra("id");
        emergencyContact = intent.getStringExtra("emergency");
    }

    @Override
    public void onResume()
    {
        super.onResume();
        startLocationUpdates();
        ConnectivityManager cm =
                (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        final boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if(isConnected){
            mFirestore=FirebaseFirestore.getInstance();
            DocumentReference citiesRef = mFirestore.collection("Users").document(intentId);
            citiesRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            String namet=(String)document.getData().get("name");
                            String mailt=(String)document.getData().get("mail");
                            long pointst=(long)document.getData().get("points");
                            name.setText(namet);
                            mail.setText(mailt);
                            points.setText(pointst+"");
                        } else {

                        }
                    } else {

                    }
                }
            });

        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        stopLocationUpdates();
    }


    @SuppressLint("MissingPermission")
    private void startLocationUpdates() {
        LocationRequest loc = new LocationRequest();
        loc.setInterval(20000);
        LocationCallback lob = new LocationCallback();
        fusedLocationClient.requestLocationUpdates(loc,lob,null);
    }

    @SuppressLint("MissingPermission")
    private void stopLocationUpdates() {
        LocationRequest loc = new LocationRequest();
        loc.setInterval(20000);
        LocationCallback lob = new LocationCallback();
        fusedLocationClient.removeLocationUpdates(lob);
    }

    public void panic(View v) {
        clickCount++;
        if(clickCount==2)
        {
            gps();
            sendMessage();
            clickCount=0;
        }
    }

    public void sendMessage(){
        SmsManager smsManager = SmsManager.getDefault();
        String message = "I'm in danger! \n Latitude: " + latitude + "\n Longitude: " + longitude;
        smsManager.sendTextMessage(emergencyContact, null, message, null, null);
    }

    public void rewards(View view)
    {
        Intent rw = new Intent(this, Rewards.class);
        rw.putExtra("name",intentName);
        rw.putExtra("mail",intentMail);
        rw.putExtra("points",intentPoints);
        rw.putExtra("emergency",emergencyContact);
        rw.putExtra("id",intentId);
        rw.putExtra("profile","pn");
        startActivity(rw);
    }

    public void connectBT(View view)
    {
        Intent bt = new Intent(this, DispositivoBT.class);
        bt.putExtra("name",intentName);
        bt.putExtra("mail",intentMail);
        bt.putExtra("points",intentPoints);
        bt.putExtra("emergency",emergencyContact);
        bt.putExtra("id",intentId);
        startActivity(bt);
    }

    public void logOut(View view)
    {
        Intent logOut = new Intent(this, MainActivity.class);
        startActivity(logOut);
    }

    @SuppressLint("MissingPermission")
    public void gps() {
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(ProfileNBT.this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                });
    }
}
