//
//  Data+CoreDataProperties.swift
//  Prototipo1
//
//  Created by Ana Quintero Ossa on 4/04/20.
//  Copyright © 2020 DAVID PATIÐO MONTOYA. All rights reserved.
//
//

import Foundation
import CoreData


extension Data {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Data> {
        return NSFetchRequest<Data>(entityName: "Data")
    }

    @NSManaged public var name: String?
    @NSManaged public var descriptionN: String?
    @NSManaged public var image: Data?
    @NSManaged public var creation: Date?

}
