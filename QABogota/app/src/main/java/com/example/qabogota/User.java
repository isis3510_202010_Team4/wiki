package com.example.qabogota;

public class User {
    private String name;
    private String mail;
    private String pass;
    private long number;
    private int points;

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }



    public User(String name, String mail, String pass, long number, int points) {
        this.name = name;
        this.mail = mail;
        this.pass = pass;
        this.number=number;
        this.points=points;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
