Android:
For using the Android version, you need to run the app on a physical device and turn on the bluetooth.
1. Pair two devices via bluetooth.
2. Run the 'Simulator' app on a physical device and turn on bluetooth.
3. Run the 'QABogota' app on a physical device and turn on bluetooth, and internet connection.
4. Register a new account with a valid phone number.
5. Login with your new account.
6. Click on the 'Connect' button.
7. Select the device where you're running the simulator.
8. Press 'Send Data' on the simulator device and see the magic!
9. If you press 'Your points' label twice, you will send and SMS to your emergency contact. 
10. If you want to test the rewards with enough points, you can login with 'test@gmail.com' and password 'test'
 