package com.example.qabogota;

import java.util.Date;

public class Data {
    private double AQ;
    private double UV;
    private double latitude;
    private double longitude;


    private Date timestamp;

    public Data(double AQ, double UV, double latitude, double longitude,Date timestamp) {
        this.AQ = AQ;
        this.UV = UV;
        this.latitude = latitude;
        this.longitude = longitude;
        this.timestamp=timestamp;
    }

    public double getAQ() {
        return AQ;
    }

    public void setAQ(double AQ) {
        this.AQ = AQ;
    }

    public double getUV() {
        return UV;
    }

    public void setUV(double UV) {
        this.UV = UV;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }



}
