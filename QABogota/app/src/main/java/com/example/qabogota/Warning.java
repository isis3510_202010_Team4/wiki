package com.example.qabogota;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;

public class Warning extends AppCompatActivity {

    private TextView warningTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_warning);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        warningTextView = findViewById(R.id.warningMessage);
        Intent intent = getIntent();
        String reason = intent.getStringExtra("reason");
        if(reason.equals("AQ")) {
            warningTextView.setText("Sensors have detected high air pollution");
        }
        else
        {
            warningTextView.setText("Sensors have detected high UV radiation");
        }
        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .8), (int) (height * .8));
    }
}
